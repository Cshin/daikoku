package com.example.shin.daikokuver166;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;

import java.util.HashMap;
import java.util.Map;

import static com.example.shin.daikokuver166.R.id.map;

//  public class Nearingshop extends FragmentActivity implements OnMapReadyCallback {

//private GoogleMap mMap;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_nearingshop);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    //@Override
   // public void onMapReady(GoogleMap googleMap) {
     //   mMap = googleMap;
        //////////////////////////////////////////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public class Nearingshop extends FragmentActivity implements OnMapReadyCallback,
                GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
                LocationListener, GoogleMap.OnMyLocationButtonClickListener, LocationSource {

            private GoogleMap mMap;
            private GoogleApiClient mGoogleApiClient;
            private LocationRequest locationRequest;
            private Map<Marker, Class> allMarkersMap = new HashMap<Marker, Class>();

            private OnLocationChangedListener onLocationChangedListener = null;

            private int priority[] = {LocationRequest.PRIORITY_HIGH_ACCURACY, LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY,
                    LocationRequest.PRIORITY_LOW_POWER, LocationRequest.PRIORITY_NO_POWER};
            private int locationPriority;
            private final int REQUEST_PERMISSION = 10;





            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_nearingshop);


//                mMap.getUiSettings().setScrollGesturesEnabled(true);



                // LocationRequest を生成して精度、インターバルを設定
                locationRequest = LocationRequest.create();

                // 測位の精度、消費電力の優先度
                locationPriority = priority[1];

                if(locationPriority == priority[0]){
                    // 位置情報の精度を優先する場合
                    locationRequest.setPriority(locationPriority);
                    locationRequest.setInterval(5000);
                    locationRequest.setFastestInterval(16);
                }
                else if(locationPriority == priority[1]){
                    // 消費電力を考慮する場合
                    locationRequest.setPriority(locationPriority);
                    locationRequest.setInterval(60000);
                    locationRequest.setFastestInterval(16);
                }
                else if(locationPriority == priority[2]){
                    // "city" level accuracy
                    locationRequest.setPriority(locationPriority);
                }
                else{
                    // 外部からのトリガーでの測位のみ
                    locationRequest.setPriority(locationPriority);
                }

                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(map);
                mapFragment.getMapAsync(this);

                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();
            }

            // onResumeフェーズに入ったら接続
            @Override
            protected void onResume() {
                super.onResume();
                mGoogleApiClient.connect();
            }

            // onPauseで切断
            @Override
            public void onPause() {
                super.onPause();
                mGoogleApiClient.disconnect();
            }

            @Override
            public void onMapReady(GoogleMap googleMap) {


                Marker markersapporotikatetu= googleMap.addMarker(new MarkerOptions().position(new LatLng(43.0675, 141.350784)).title("ダイコクドラッグ札幌地下鉄駅前店"));
                allMarkersMap.put(markersapporotikatetu, Sapporotikatetuekimae.class);
               //googleMap.setOnInfoWindowClickListener((OnInfoWindowClickListener) this);

                Marker markerminaminijyou= googleMap.addMarker(new MarkerOptions().position(new LatLng(43.058534, 141.356095)).title("ダイコクドラッグ南二条店"));
                allMarkersMap.put(markerminaminijyou, Minaminijyou.class);


                Marker markersusukinoekimae = googleMap.addMarker(new MarkerOptions().position(new LatLng(43.056146, 141.352995)).title("ダイコクドラッグすすきの駅前店"));
                allMarkersMap.put(markersusukinoekimae, Susukinoekimae.class);

                Marker markerkiraku = googleMap.addMarker(new MarkerOptions().position(new LatLng(43.056933, 141.351581)).title("ダイコクドラッグ狸小路キラク店"));
                allMarkersMap.put(markerkiraku, Kiraku.class);
        //        googleMap.addMarker(new MarkerOptions().position(new LatLng(43.058534, 141.356095)).title("ダイコクドラッグ南二条店"));
                googleMap.addMarker(new MarkerOptions().position(new LatLng(43.195826, 141.006135)).title("ダイコクドラッグ小樽運河店"));
                googleMap.addMarker(new MarkerOptions().position(new LatLng(43.194630, 141.006876)).title("ダイコクドラッグ小樽花畑店"));



                googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

                    public void onInfoWindowClick(Marker marker) {
                        Class cls = allMarkersMap.get(marker);
                        Intent intent = new Intent(Nearingshop.this, cls);
                        startActivity(intent);

                    }

                });


                LinearLayout homepage = (LinearLayout)findViewById(R.id.homepage);
                homepage.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View v){
                        Intent intent = new Intent(Nearingshop.this, MainActivity.class);
                        startActivity(intent);
                    }
                });



                // check permission
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.d("debug", "permission granted");

                    mMap = googleMap;
                    // default の LocationSource から自前のsourceに変更する
                    mMap.setLocationSource(this);
                    mMap.setMyLocationEnabled(true);
                    mMap.setOnMyLocationButtonClickListener(this);
                }
                else{
                    Log.d("debug", "permission error");
                    return;
                }
            }

            @Override
            public void onLocationChanged(Location location) {
                Log.d("debug","onLocationChanged");
                if (onLocationChangedListener != null) {
                    onLocationChangedListener.onLocationChanged(location);

                    double lat = location.getLatitude();
                    double lng = location.getLongitude();

                    Log.d("debug","location="+lat+","+lng);

                    Toast.makeText(this, "location="+lat+","+lng, Toast.LENGTH_SHORT).show();

                    // Add a marker and move the camera
                    LatLng newLocation = new LatLng(lat, lng);


                    mMap.addMarker(new MarkerOptions().position(newLocation).title("現在地").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));

                }

            }





        @Override

        public boolean onTouchEvent(MotionEvent event) {

            boolean mIsTouchEnded;
            boolean mIsScrolling;
            if (event.getAction() == MotionEvent.ACTION_DOWN){

                mIsTouchEnded = false;

            } else if (event.getAction() == MotionEvent.ACTION_UP){

                mIsTouchEnded = true;

            } else if (event.getAction() == MotionEvent.ACTION_MOVE){

                mIsScrolling = true;

            }

            return super.onTouchEvent(event);

        }




        @Override
            public void onConnected(Bundle bundle) {
                // check permission
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Log.d("debug", "permission granted");

                    // FusedLocationApi
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, locationRequest, this);
                }
                else{
                    Log.d("debug", "permission error");
                    return;
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d("debug", "onConnectionSuspended");
            }

            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.d("debug", "onConnectionFailed");
            }

            @Override
            public boolean onMyLocationButtonClick() {
                Toast.makeText(this, "onMyLocationButtonClick", Toast.LENGTH_SHORT).show();

                return false;
            }

            // OnLocationChangedListener calls activate() method
            @Override
            public void activate(OnLocationChangedListener onLocationChangedListener) {
                this.onLocationChangedListener = onLocationChangedListener;
            }

            @Override
            public void deactivate() {
                this.onLocationChangedListener = null;
            }






        }



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Add a marker in Sydney and move the camera
       // LatLng sydney = new LatLng(-34, 151);
       // mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
       // mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));



