package com.example.shin.daikokuver166;

import android.content.Intent;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.google.android.gms.vision.text.Line;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private ViewFlipper viewFlipper;
    private GestureDetector gestureDetector;
    private Map<LinearLayout, Class> alllayoutmap = new HashMap<LinearLayout, Class>();
    public static int permissionflag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        viewFlipper = (ViewFlipper) findViewById(R.id.flipper);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        viewFlipper.setAutoStart(true);     //自動でスライドショーを開始
        viewFlipper.setFlipInterval(5000);


        LinearLayout renqishangpin = (LinearLayout)findViewById((R.id.renqishangpin));
        LinearLayout nearingshop = (LinearLayout)findViewById(R.id.nearingshop);
        LinearLayout tuijianshangpin = (LinearLayout)findViewById(R.id.tuijianshangpin);
        LinearLayout cuxiaoqingbao = (LinearLayout)findViewById((R.id.cuxiaoqingbao));
        LinearLayout dianpusousuo = (LinearLayout)findViewById(R.id.dianpusousuo);
        LinearLayout youhuijuan = (LinearLayout)findViewById(R.id.youhuijuan);
        LinearLayout bendichangxiao = (LinearLayout)findViewById(R.id.bendichangxiao);
        alllayoutmap.put(renqishangpin, Renqishangpin.class);
        alllayoutmap.put(nearingshop, Nearingshop.class);
        alllayoutmap.put(tuijianshangpin, Tuijianshangpin.class);
        alllayoutmap.put(cuxiaoqingbao, Cuxiaoqingbao.class);
        alllayoutmap.put(dianpusousuo, Dianpusousuo.class);
        alllayoutmap.put(youhuijuan, Youhuijuan.class);
        alllayoutmap.put(bendichangxiao, Bendichangxiao.class);
        nearingshop.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        bendichangxiao.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        renqishangpin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        youhuijuan.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        tuijianshangpin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        cuxiaoqingbao.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });
        dianpusousuo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Class cls = alllayoutmap.get(v);
                Intent intent = new Intent(MainActivity.this, cls);
                startActivity(intent);
            }
        });


        ImageButton back = (ImageButton)findViewById(R.id.imageButtonback);
        ImageButton next = (ImageButton)findViewById( R.id.imageButtonnext);


        back.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                viewFlipper.showPrevious();
            }
        });
        next.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                viewFlipper.showNext();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        gestureDetector = new GestureDetector(this, simpleOnGestureListener);
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch ( event.getAction() ) {

            case MotionEvent.ACTION_DOWN:
                //画面がタッチされたときの動作
                break;

            case MotionEvent.ACTION_MOVE:
                //タッチしたまま移動したときの動作
                break;

            case MotionEvent.ACTION_UP:
                //タッチが離されたときの動作
                break;

            case MotionEvent.ACTION_CANCEL:
                //他の要因によってタッチがキャンセルされたときの動作
                break;

        }

        //return super.onTouchEvent(event);
        return gestureDetector.onTouchEvent(event);
    }



    private final GestureDetector.SimpleOnGestureListener simpleOnGestureListener = new GestureDetector.SimpleOnGestureListener()
    {
        public boolean onFling(MotionEvent e1 // TouchDown時のイベント
                , MotionEvent e2   // TouchDown後、指の移動毎に発生するイベント
                , float velocityX  // X方向の移動距離
                , float velocityY)  // Y方向の移動距離
        {
            // 絶対値の取得
            float dx = Math.abs(velocityX);
            float dy = Math.abs(velocityY);
            // 指の移動方向(縦横)および距離の判定
            if (dx > dy && dx > 300) {
                // 指の移動方向(左右)の判定
                if (e1.getX() < e2.getX()) {
                    viewFlipper.showPrevious();
                } else {
                    viewFlipper.showNext();
                }
                return true;
            }
            return false;
        }
    };
    public void onClick(View v) {
        switch(v.getId()){


            case R.id.imageButtonback:
                viewFlipper.showPrevious();
                break;

            case R.id.imageButtonnext:
                viewFlipper.showNext();
                break;

            default:
                viewFlipper.setAutoStart(true);     //自動でスライドショーを開始
                viewFlipper.setFlipInterval(5000);
                break;
        }
    }


}
